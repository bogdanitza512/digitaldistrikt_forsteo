﻿/// <summary>
/// A base menu class that implements parameterless Show and Hide methods
/// </summary>
public abstract class SimpleSlide<T> : Slide<T> where T : SimpleSlide<T>
{
	public static void Show()
	{
		Open();
	}

	public static void Hide()
	{
		Close();
	}
}
