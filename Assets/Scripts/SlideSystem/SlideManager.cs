﻿using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class SlideManager : MonoBehaviour
{

    public HomeSlide homeSlide;
    public TreatmentParadigmSlide TreatmentParadigmSlide;
    public MOASlide MOASlide;
    public EfficacySlide EfficacySlide;
    public EfficacyDesignSlide EfficacyDesignSlide;
    public EfficacyACR50Slide EfficacyACR50Slide;
    public EfficacyACR50FastSlide EfficacyACR50FastSlide;
    public EfficacyACR50SustainedSlide EfficacyACR50SustainedSlide;
    public EfficacyInhibitionSlide EfficacyInhibitionSlide;
    public ClinicalStudiesSlide ClinicalStudiesSlide;
    public ClinicalStudiesStudyProgramsSlide ClinicalStudiesStudyProgramsSlide;
    public ClinicalStudiesEfficacySlide ClinicalStudiesEfficacySlide;
    public SafetySlide SafetySlide;
    public SafetyMalignancySlide SafetyMalignancySlide;
    public SafetyMalignancyLongTermSlide SafetyMalignancyLongTermSlide;
    public SafetyMalignancyPlaceboSlide SafetyMalignancyPlaceboSlide;
    public SafetyInfectionsSlide SafetyInfectionsSlide;
    public SafetyHerpesSlide SafetyHerpesSlide;
    public SafetyHerpesLongTermSlide SafetyHerpesLongTermSlide;
    public SafetyHerpesPlaceboSlide SafetyHerpesPlaceboSlide;

    public ReferenceSlide ReferenceSlide;
    public RCPSlide RCPSlide;

    public Stack<Slide> slideStack = new Stack<Slide>();

    public static SlideManager Instance { get; set; }

    private void Awake()
    {
        Instance = this;
        HomeSlide.Show();
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    public void CreateInstance<T>() where T : Slide
	{
		var prefab = GetPrefab<T>();

        Instantiate(prefab,transform);
	}

    public void OpenSlide(Slide instance)
    {
        // De-activate top menu
        if (slideStack.Count > 0)
        {
            if (instance == slideStack.Peek())
            {
                return;
            }
			if (instance.DisableMenusUnderneath)
			{
                foreach (var slide in slideStack)
				{
					slide.gameObject.SetActive(false);

					if (slide.DisableMenusUnderneath)
						break;
				}
			}
        }

        slideStack.Push(instance);
    }

    private T GetPrefab<T>() where T : Slide
    {
        // Get prefab dynamically, based on public fields set from Unity
		// You can use private fields with SerializeField attribute too
        var fields = this.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
        foreach (var field in fields)
        {
            var prefab = field.GetValue(this) as T;
            if (prefab != null)
            {
                return prefab;
            }
        }

        throw new MissingReferenceException("Prefab not found for type " + typeof(T));
    }
	
    public void CloseSlide(Slide slide)
	{
		if (slideStack.Count == 0)
		{
			Debug.LogErrorFormat(slide, "{0} cannot be closed because menu stack is empty", slide.GetType());
			return;
		}

		if (slideStack.Peek() != slide)
		{
			Debug.LogErrorFormat(slide, "{0} cannot be closed because it is not on top of stack", slide.GetType());
			return;
		}
        if(slideStack.Count == 1)
        {
            //landingPage.TryPerformTransitionTo(LandingPageSlide.LandingPageState.Idle);
        }

		CloseTopSlide();
	}

    public void CloseTopSlide()
    {
        
        var instance = slideStack.Peek();

        if (instance is HomeSlide)
        {
            print("home run");
            instance.gameObject.SetActive(true);
            return;
        }
        slideStack.Pop();

		if (instance.DestroyWhenClosed)
        	Destroy(instance.gameObject);
		else
			instance.gameObject.SetActive(false);

        // Re-activate top menu
		// If a re-activated menu is an overlay we need to activate the menu under it
        foreach (var slide in slideStack)
		{
            slide.gameObject.SetActive(true);

			if (slide.DisableMenusUnderneath)
				break;
		}
    }

    public void CloseAllSlides()
    {
        while(slideStack.Count != 0)
        {
            var instance = slideStack.Pop();
            Destroy(instance.gameObject);
        }
    }

    private void Update()
    {
        // On Android the back button is sent as Esc
        if (Input.GetKeyDown(KeyCode.Escape) && slideStack.Count > 0)
        {
            slideStack.Peek().OnBackPressed();

        }
    }

    public void OnBackButton_Pressed()
    {
        slideStack.Peek().OnBackPressed();
    }
}

