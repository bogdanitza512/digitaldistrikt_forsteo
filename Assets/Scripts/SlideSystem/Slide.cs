﻿using Sirenix.OdinInspector;
using UnityEngine;

public abstract class Slide<T> : Slide where T : Slide<T>
{
    public static T Instance { get; private set; }

    protected virtual void Awake()
    {
        Instance = (T)this;
    }

    protected virtual void OnDestroy()
    {
        Instance = null;
	}

	protected static void Open()
	{
		if (Instance == null)
			SlideManager.Instance.CreateInstance<T>();
		else
			Instance.gameObject.SetActive(true);
		
        SlideManager.Instance.OpenSlide(Instance);
        Instance.OnOpen();
	}

	protected static void Close()
	{
		if (Instance == null)
		{
			Debug.LogErrorFormat("Trying to close menu {0} but Instance is null", typeof(T));
			return;
		}

        SlideManager.Instance.CloseSlide(Instance);
        Instance.OnClose();
	}

    public override void OnBackPressed()
	{
		Close();
	}

	public override void OnOpen()
	{
        
	}

    public override void OnClose()
    {
        
    }
}

public abstract class Slide : SerializedMonoBehaviour
{
	[Tooltip("Destroy the Game Object when menu is closed (reduces memory usage)")]
	public bool DestroyWhenClosed = true;

	[Tooltip("Disable menus that are under this one in the stack")]
	public bool DisableMenusUnderneath = true;

	public abstract void OnBackPressed();

    public abstract void OnOpen();

    public abstract void OnClose();
}
