﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class PlayerController : MonoBehaviour
{


    #region Fields and Properties

    [SerializeField]
    private KeyCode exitKey = KeyCode.Q;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
        Screen.SetResolution(width: 1920, height: 1080,
                             fullscreen: true);
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if (Input.GetKeyDown(exitKey))
        {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
            print("Unity.Editor::Playmode stopped...");
        #else
            Application.Quit();
        #endif
        }
    }

#endregion

#region Methods

	

#endregion
}
