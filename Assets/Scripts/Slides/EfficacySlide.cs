﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class EfficacySlide : SimpleSlide<EfficacySlide>
{

    #region Fields and Properties

    [SerializeField]
    Animator graphAnimator;

    [SerializeField]
    string openTriggerName;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
	
    }

	#endregion

	#region Methods

	public override void OnOpen()
	{
        base.OnOpen();
        graphAnimator.SetTrigger(openTriggerName);
	}

	public void OnDesignButton_Pressed()
    {
        EfficacyDesignSlide.Show();
    }

    public void OnACR50Button_Pressed()
    {
        EfficacyACR50Slide.Show();
    }

    public void OnInhibitionButton_Pressed()
    {
        EfficacyInhibitionSlide.Show();
    }

    #endregion
}
