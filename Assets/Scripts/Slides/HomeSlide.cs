﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class HomeSlide : SimpleSlide<HomeSlide>
{

    #region Fields and Properties


    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
	
    }

	#endregion

	#region Methods

	public override void OnOpen()
	{
        base.OnOpen();
        NavbarManager.Instance.TryTransitionTo(NavbarManager.NavbarState.Opened);
	}

	public override void OnBackPressed()
	{
        MasterSlide.Instance.TryPerformTransitionTo(MasterSlide.TabsState.Idle);
	}

	#endregion
}
