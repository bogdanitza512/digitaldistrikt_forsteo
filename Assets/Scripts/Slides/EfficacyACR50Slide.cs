﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class EfficacyACR50Slide : SimpleSlide<EfficacyACR50Slide>
{

    #region Fields and Properties

	

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
      
    }

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	void Update()
    {
	
    }

    #endregion

    #region Methods

    public override void OnOpen()
    {
        base.OnOpen();
        EfficacyACR50FastSlide.Show();
    }

	public override void OnClose()
	{
        base.OnClose();
        EfficacyACR50SustainedSlide.Close();
        EfficacyACR50SustainedSlide.Close();
	}

	public void OnFastButton_Pressed()
    {
        if(EfficacyACR50SustainedSlide.Instance != null)
            EfficacyACR50SustainedSlide.Hide();
        EfficacyACR50FastSlide.Show();

    }
	
    public void OnSustainedButton_Pressed()
    {
        if (EfficacyACR50FastSlide.Instance != null)
            EfficacyACR50FastSlide.Hide();
        EfficacyACR50SustainedSlide.Show();

    }


	#endregion
}
