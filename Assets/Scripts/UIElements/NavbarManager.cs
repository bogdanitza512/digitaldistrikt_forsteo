﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Extensions;

/// <summary>
/// 
/// </summary>
[RequireComponent(typeof(Animator))]
public class NavbarManager : SerializedMonoSingleton<NavbarManager>
{

    #region Fields and Properties

    [SerializeField]
    Animator anim;

    public enum NavbarState{ Opened, Closed, }
    [SerializeField]
    Dictionary<NavbarState, string> navbarStateTriggerNameMap 
        = new Dictionary<NavbarState, string>();

    [SerializeField]
    NavbarState lastState = NavbarState.Opened;

    public int someInt;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        if(anim == null)
        {
            anim = GetComponent<Animator>();
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
	
    }

    #endregion

    #region Methods

	public void OnMenuButton_Pressed()
    {
        var succes = TryTransitionTo(NavbarState.Opened);
        print(succes);
        if (succes)
        {
            //...
        }
    }

    public void OnCloseButton_Pressed()
    {
        var succes = TryTransitionTo(NavbarState.Closed);
        print(succes);
        if (succes)
        {
            //... 
        }
    }

    public void OnParadigmButton_Pressed()
    {
        if(SlideManager.Instance.slideStack.Peek() is TreatmentParadigmSlide)
        {
            print("same slide");
            TryTransitionTo(NavbarState.Closed);
            return;
        }
        if (SlideManager.Instance.slideStack.Count >= 2)
            SlideManager.Instance.CloseTopSlide();
        TreatmentParadigmSlide.Show();
        TryTransitionTo(NavbarState.Closed);
    }

    public void OnMOAButton_Pressed()
    {
        if (SlideManager.Instance.slideStack.Peek() is MOASlide)
        {
            print("same slide");
            TryTransitionTo(NavbarState.Closed);
            return;
        }
        if (SlideManager.Instance.slideStack.Count >= 2)
            SlideManager.Instance.CloseTopSlide();
        MOASlide.Show();
        TryTransitionTo(NavbarState.Closed);
    }

    public void OnEfficacyButton_Pressed()
    {
        if (SlideManager.Instance.slideStack.Peek() is EfficacySlide)
        {
            print("same slide");
            TryTransitionTo(NavbarState.Closed);
            return;
        }
        if (SlideManager.Instance.slideStack.Count >= 2)
            SlideManager.Instance.CloseTopSlide();
        EfficacySlide.Show();
        TryTransitionTo(NavbarState.Closed);
    }

    public void OnClinicalStudyButton_Pressed()
    {
        if (SlideManager.Instance.slideStack.Peek() is ClinicalStudiesSlide)
        {
            print("same slide");
            TryTransitionTo(NavbarState.Closed);
            return;
        }
        if(SlideManager.Instance.slideStack.Count >= 2)
            SlideManager.Instance.CloseTopSlide();
        ClinicalStudiesSlide.Show();
        TryTransitionTo(NavbarState.Closed);
    }

    public void OnSafetyButton_Pressed()
    {
        if (SlideManager.Instance.slideStack.Peek() is SafetySlide)
        {
            print("same slide");
            TryTransitionTo(NavbarState.Closed);
            return;
        }
        if (SlideManager.Instance.slideStack.Count >= 2)
            SlideManager.Instance.CloseTopSlide();
        SafetySlide.Show();
        TryTransitionTo(NavbarState.Closed);
    }

    public void OnHomeButton_Pressed()
    {
        if (SlideManager.Instance.slideStack.Peek() is HomeSlide)
        {
            print("same slide");
            TryTransitionTo(NavbarState.Closed);
            return;
        }
        if (SlideManager.Instance.slideStack.Count >= 2)
            SlideManager.Instance.CloseTopSlide();
        HomeSlide.Show();
        TryTransitionTo(NavbarState.Closed);
    }

    public bool TryTransitionTo(NavbarState newState)
    {
        if(lastState != newState)
        {
            anim.SetTrigger(navbarStateTriggerNameMap[newState]);
            print("{0}:{1}".FormatWith(lastState, newState));
            lastState = newState;
            return true;
        }
        print("{0}:{1}".FormatWith(lastState, newState));
        return false;
    }

    #endregion
}
