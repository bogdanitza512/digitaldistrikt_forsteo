﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;
using System.Reflection;

/// <summary>
/// 
/// </summary>
[RequireComponent(typeof(RectTransform))]
public class SwitchState : MonoBehaviour
{

    #region Fields and Properties

    [OnValueChanged("OnFieldChange")]
    public RectTransform rectTransform;

    [OnValueChanged("OnFieldChange")]
    public Button button;

    [OnValueChanged("OnFieldChange")]
    public TMP_Text text;

    public Vector2 pos
    {
        get { return rectTransform.anchoredPosition; }
    }

    public Color color
    {
        get { return text.color; }
        set { text.color = value; }
    }

    #endregion

    #region Unity Messages

    private void Awake()
    {
        if (rectTransform == null)
        {
            rectTransform = gameObject.GetComponent<RectTransform>();
        }

        if (button == null)
        {
            button = gameObject.GetComponent<Button>();
        }

        if(text == null)
        {
            text = gameObject.GetComponentInChildren<TMP_Text>();
        }
    }

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	void Update()
    {
	
    }

    #endregion

    #region Methods

	void OnFieldChange()
    {
        bool requireDebugPrint = false;

        if(rectTransform == null)
        {
            if (button != null)
            {
                rectTransform = button.gameObject.GetComponent<RectTransform>();
                requireDebugPrint = true;
            }
        }

        if(button == null)
        {
            if (rectTransform != null)
            {
                button = rectTransform.GetComponent<Button>();
                if (button == null)
                    button = rectTransform.GetComponentInChildren<Button>();
                requireDebugPrint = true;
            }
            else if (text != null)
            {
                button = text.GetComponentInParent<Button>();
                requireDebugPrint = true;
            }
        }

        if(text == null)
        {
            if(rectTransform != null)
            {
                text = button.GetComponentInChildren<TMP_Text>();
                requireDebugPrint = true;
            }
            else if (button != null)
            {
                text = button.GetComponentInChildren<TMP_Text>();
                requireDebugPrint = true;
            }
        }
        if(requireDebugPrint)
        {
            print("Switch state updated.");
        }

    }

    #endregion
}
